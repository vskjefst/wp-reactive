'use strict';

var gulp = require('gulp');
var watch = require('gulp-watch');
var batch = require('gulp-batch');
var browserify = require('gulp-browserify');

gulp.task('default', ['copy','browserify','watch']);

gulp.task('copy', function() {
    gulp.src('src/html/**')
    .pipe(watch('src/html/**'))
    .pipe(gulp.dest('dist'))
});

gulp.task('watch', function () {
    watch('src/js/main.js', batch(function (events, done) {
        gulp.start('browserify', done);
    }));
});

gulp.task('browserify', function() {
    return gulp.src('src/js/main.js')
    .pipe(browserify({
      insertGlobals: true
    }))
    .pipe(gulp.dest('dist/js'));
});
